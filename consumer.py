from manager import RabbitManager


class ConsumerManager(RabbitManager):
    def __init__(self, host, port, user, passw, exchange, routing_key, callback, queue=''):
        super(ConsumerManager, self).__init__(host, port, user, passw)
        self.connect()
        self.exchange = exchange
        self.routing_key = routing_key
        self.set_channel()
        result = self.channel.queue_declare(exclusive=True, durable=True, queue=queue)
        self.queue_name = result.method.queue
        self.binding()
        self.callback = callback

    def binding(self):
        self.channel.queue_bind(exchange=self.exchange,
                                queue=self.queue_name,
                                routing_key=self.routing_key)  # class.*.*

    def start(self):
        self.channel.basic_qos(prefetch_count=1)  # don't give work to consumer until finishes
        self.channel.basic_consume(self.callback,
                                   queue=self.queue_name)
        self.channel.start_consuming()

    def stop(self):
        self.channel.stop_consuming()


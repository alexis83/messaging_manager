import pika


class RabbitManager(object):
    def __init__(self, host, port, user, passw):
        self.host = host
        self.port = port
        self.user = user
        self.passw = passw
        self.connection = None
        self.channel = None
        self.exchange = None
        self.routing_key = None

    def connect(self):
        credentials = pika.PlainCredentials(self.user, self.passw)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.host,
                                                                            port=self.port,
                                                                            credentials=credentials))
        self.channel = self.connection.channel()

    def set_channel(self):
        self.channel.exchange_declare(exchange=self.exchange,
                                      type='topic')

    def check(self):
        if self.connection.is_closed or self.connection.is_closing \
                or self.channel.is_closed or self.channel.is_cloing:
            self.connect()
        while self.connection.is_closed and self.channel.is_closed:
            True

    def close(self):
        self.channel.close()
        self.connection.close()

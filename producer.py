from manager import RabbitManager
import pika


class ProducerManager(RabbitManager):
    def __init__(self, host, port, user, passw, exchange, routing_key):
        super().__init__(host, port, user, passw)
        self.exchange = exchange
        self.routing_key = routing_key
        self.connect()
        self.set_channel()

    def publish(self, message):
        if not self.connection.is_open or not self.channel.is_open:
            self.check()
        self.channel.basic_publish(exchange=self.exchange,
                                   routing_key=self.routing_key,
                                   body=message)

    def close(self):
        super().close()

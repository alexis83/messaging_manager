### Messaging Manager ###

This project is in charge of the management for consuming and publish messages through a message broker.

It has a main class called RabbitManager and two child classes called ProducerManager and ConsumerManager

* Version: 1.0

### Dependencies ###

* [Pika](http://pika.readthedocs.io/en/0.10.0/)

### Who do I talk to? ###

* aflores@cathedralsw.com
from consumer import ConsumerManager


def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))
    ch.basic_ack(delivery_tag=method.delivery_tag)


myconsumer = ConsumerManager('localhost', 5672, 'test', 'test', 'exchange_class', 'class.*', callback)

print("Consuming... ")
myconsumer.start()

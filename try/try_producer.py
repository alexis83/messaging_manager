from producer import ProducerManager
import time

myproducer1 = ProducerManager('localhost', 5672, 'test', 'test', 'exchange_class', 'class.34')
myproducer2 = ProducerManager('localhost', 5672, 'test', 'test', 'exchange_notes', 'notes.34')

while True:
    time.sleep(3)
    myproducer1.publish("Christmas Party tomorrow at 5 - {}".format(time.time()))
    myproducer2.publish("Studen X - {}".format(time.time()))
    print("Published")
